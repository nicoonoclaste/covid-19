#!/usr/bin/env python3

import click
import pandas as pd

import lzma
from pathlib import Path

@click.command()
@click.argument("inpath", type=click.Path(exists=True),
                default="./latest.xslx.lzma")
def main(inpath):
    inpath = Path(inpath)
    with lzma.open(inpath, 'rb') as infile:
        df = pd.read_excel(infile, sheet_name="COVID-19-geographic-disbtributi")

    df, countries = reshape(df)

    def path(name):
        return inpath.with_suffix(f'.{name}.parquet')

    df.to_parquet(path('data'), compression='zstd')
    countries.to_parquet(path('countries'), compression='zstd')

def reshape(df):
    # Remove redundant columns
    assert 'dateRep' in df
    df = df.drop(columns=['day', 'month', 'year'])

    # Reshape data
    cName = 'countriesAndTerritories'
    df[cName] = df[cName].astype('category')
    df.set_index([cName, 'dateRep'], inplace=True)
    df.sort_index(level=[cName, 'dateRep'], inplace=True)

    # Split out country metadata
    cMeta = ['countryterritoryCode', 'geoId', 'popData2018']
    countries = df.filter(axis='columns', items=cMeta)
    countries.drop_duplicates(inplace=True)
    countries.reset_index(level='dateRep', drop=True, inplace=True)
    df.drop(columns=cMeta, inplace=True)

    # Add cummulative columns
    df['cumCases'] = df.cases.groupby('countriesAndTerritories').cumsum()
    df['cumDeaths'] = df.deaths.groupby('countriesAndTerritories').cumsum()

    # Find time of 100th case and death, for each country
    countries['case100'] = df.cumCases[df.cumCases >= 100].groupby(cName).idxmax().map(lambda x: x[1])
    countries['death100'] = df.cumDeaths[df.cumDeaths >= 100].groupby(cName).idxmax().map(lambda x: x[1])

    return df, countries


if __name__ == "__main__":
    main()
