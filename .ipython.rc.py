from process import reshape

import pandas as pd

import lzma

with lzma.open('latest.xlsx.lzma', 'rb') as infile:
    df = pd.read_excel(infile, sheet_name="COVID-19-geographic-disbtributi")

df, countries = reshape(df)
cName = 'countriesAndTerritories'
